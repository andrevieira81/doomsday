package doomsday;
import robocode.*;
import java.awt.Color;

/**
 * Doomsday - a robot by (André Nascimento Vieira)
 */
public class Doomsday extends Robot
{
	/**
	 * run: Doomsday's robot
	 */
public void run() {

	   setColors(Color.black,Color.blue,Color.black); // Configuração da arma, radar e corpo

		// Iniciando as ações do robô
while(true)       {
       ahead(100);         //Anda para frente 100 pixels
       turnGunRight(360);  //Gira a arma 360º para a direita
	   turnGunLeft(360);   //Gira a arma 360º para a esquerda
	   ahead(80);          //Anda para frente 100 pixels
	   turnGunLeft(360);   //Gira a arma 360º para a esquerda
       back(75);           //Vai para trás 75 pixels
	   ahead(80);          //Anda para frente 100 pixels
       turnGunRight(360);  //Gira a arma 360º para a direita
	              }

}

	/**
	 * onScannedRobot: O que o robô irá fazer, quando ver outro robô
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// As linhas abaixo referem-se, a decisões tomadas pelo robô, de acordo com a distância do alvo 
    double distance = e.getDistance(); //Obtém a distância do robô escaneado
    if(distance > 800) //Se a distância for maior que 800px, o tiro é do tipo "1"
       fire(1);
    else if(distance > 600 && distance <= 800) //O robô irá checkar a distância do robô escaneado se é maior que 600px e se for menor igual a 800px, o tiro é "2"
       fire(2);
    else if(distance > 400 && distance <= 600) //O robô irá checkar a distância do robô escaneado se é maior que 400px e se for menor igual a 600px, o tiro é "2"
       fire(2);
    else if(distance > 200 && distance <= 400) //O robô irá checkar a distância do robô escaneado se é maior que 200px e se for menor igual a 600px, o tiro é "2"
       fire(2);
    else if(distance < 200)                    //O robô irá checkar a distância do robô escaneado se é menor que 200px, então o tiro será "3"
       fire(3);
}

	/**
	 * onHitByBullet: O que o robô irá fazer, se for acertado...
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// O robô irá para trás 10px
		back(10);
    }
	
	/**
	 * onHitWall: O que o robô irá fazer, se bater na parede...
	 */
	public void onHitWall(HitWallEvent e)         {
		double bearing = e.getBearing(); //se pegar o rolamento na parede...
       turnRight(-bearing); //Robo irá virar à direita
       ahead(100);          //O robô se afastará da parede 100px
	}
		
}
