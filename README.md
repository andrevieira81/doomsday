<h2>Robo Doomsday</h2>

<h2>Descrição do robô:</h2>

<p>
<h3>O Robô foi criado para identificar o alvo, a uma certa distância e de acordo com a distância do alvo, ele irá decidir o tipo de tiro que o arma executará.</h3>
<h3>quanto maior a distância do alvo ele irá dar tiro na categoria "1" se estiver menos de 200 pixels, ele irá dar tiro "3"</h3>
<h3>A arma irá girar 360º à direita ou a esquerda dependendo de sua orientação na tela.</h3>
<h3>Caso ele seja acertado irá dar um passo de 10 pixels</h3>
<h3>Se ele atigir a parede, ele irá se afastar efetuando um movimento contrário</h3>
<h3>O robô efetua movimentos de ir para a frente e girar a arma e efetuar movimentos de 90 graus, para a esquerda e a direita</h3>
</p>

<p><h2>Pontos forte:</h2></p>
<h3>Pode esquivar se estiver no meio de um fogo cruzado (pelo menos nos testes!!!) devido o tipo de movimentação ele executa</h3>
<h3>Se estiver muito proximo de um alvo, ele acerta com tiro 3</h3>
<h3>Dificilmente ele se bate na parede, pois se atingir uma parede, ele executa um movimento de "deslize na parede"</h3>

<p><h2>Ponto fracos:</h2></p>
<h3>Erra muito o alvo de longe</h3>
<h3>Se ele estiver pressionado por outro robô pode ser morto facilmente, porque ele estará mirando em um robô mais próximo e não irá focar no que estiver atacando</h3>

<h3>Agradeço ao time da Solutis, pela oportunidade de avaliação do meu robô criei. Para mim foi bastante proveitoso e me fez relembrar um pouco de java, gostaria de ter feito mais e me incentivou a estudar mais!</h3>

